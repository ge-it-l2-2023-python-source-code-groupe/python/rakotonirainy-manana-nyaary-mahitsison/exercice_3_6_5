perc_GC = ((4500+2575)/14800)*100

#Ecriture formate
print("Ecriture formate ")
print("Le pourcentage de GC est :{:.0f} %".format(perc_GC),"\nLe pourcentage de GC est :{:.1f} %".format(perc_GC),"\nLe pourcentage de GC est: {:.2f} %".format(perc_GC),"\nLe pourcentage de GC est: {:.3f} %".format(perc_GC))

#Ecriture f-string
print("\nEcriture f-string")
print(f"Le pourcentage de GC est {perc_GC:.0f} %\nLe pourcentage de GC est {perc_GC:.1f} % \nLe pourcentage de GC est {perc_GC:.2f} %  \nLe pourcentage de GC est {perc_GC:.3f} %")